const express = require("express");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const routing = require("./kernel/routing");
const { cors } = require("./kernel/cors");
const dotenv = require("dotenv");
const path = require("path");
dotenv.config();
const app = express();

//middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(helmet()); //to secure http header

//handle database
require("./kernel/database");
//handle cors
app.use(cors);

//handle routing in components
routing.then((routes) => {
    for (var i = 0; i < routes.length; i++) {
        //add routes
        app.use("/" + routes[i].owner, routes[i].instance);
    }

    //handle route error
    app.all('*', (req, res) => {
        throw new Error("This route is not found !!!");
    })

    app.use((err, req, res, next) => {
        if (err.message === "This route is not found !!!") {
            res.status(404).json({ error: { msg: err.message } });
        }
    });

}).catch(error => {
    console.log("\x1b[31m", error.stack);
    console.log("\x1b[0m");
});

module.exports = app;