const mongoose = require("mongoose");

// Person model
const PersonSchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    lastname: {
        type: String,
        required: true,
        match: /^[a-z ,.'-]+$/i
    },
    firstname: {
        type: String,
        required: true,
        match: /^[a-z ,.'-]+$/i

    },
    birthday: {
        type: Date,
        required: true
    },
    sexe: {
        type: String,
        enum : ['male','femelle'],
        reauired: true
    }
});

module.exports = mongoose.model("Person", PersonSchema);