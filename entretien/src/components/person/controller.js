const Person = require("./model");
const mongoose = require("mongoose");
const e = require("express");

exports.create = (req, res) => {
    if(new Date() >= new Date(req.body.birthday)) {
        const person = new Person({...req.body, _id: mongoose.Types.ObjectId()});
        person.save().then(result => {
            res.status(201).json(result)
        }).catch(err => {
            res.status(500).json({
                error: err.message
            });
        });
    }  else {
        res.status(500).json({error: "The birthday can't in the future."})
    }
}

exports.update = (req, res) => {
    Person.findById(req.params.id).exec()
        .then(person => {
            if (!person) return res.status(404).json({ error: "404 not found !!!" });
            if( req.body.birthday && new Date() < new Date(req.body.birthday)) return res.status(500).json({error: "The birthday can't in the future."});
            Person.updateOne({ _id: req.params.id }, req.body).exec()
                .then(() => {
                    res.status(200).json({ message: "this item is updated successfully." });
                }).catch(err => {
                    res.status(500).json({
                        error: err.message
                    });
                });
           
        })
}

exports.delete = (req, res) => {
    Person.deleteOne({_id: req.params.id}).exec()
        .then(() => {
            res.status(200).json({message: "this item is deleted successfully."})
        }).catch(err => {
            res.status(500).json({
                error: err.message
            });
        });
}

exports.getPersonById = (req, res) => {
    Person.findById(req.params.id).exec()
        .then(result => {
            res.status(200).json(result);
        }).catch(err => {
            res.status(500).json({
                error: err.message
            });
        });
}

exports.getPersons = (req, res) => {
    Person.find().exec()
        .then(results => {
            res.status(200).json(results);
        }).catch(err => {
            res.status(500).json({
                error: err.message
            });
        });
}