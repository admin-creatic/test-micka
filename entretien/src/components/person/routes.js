const express = require("express");
const Router = express.Router();
const controller = require("./controller");

Router.get("/", controller.getPersons);
Router.get("/:id", controller.getPersonById);
Router.post("/", controller.create);
Router.patch("/:id", controller.update);
Router.delete("/:id", controller.delete);

module.exports = Router;

