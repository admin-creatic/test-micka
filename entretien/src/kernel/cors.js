
// it's q middleware to handle CORS
module.exports.cors = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*"); // default allow all clients 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization"); // list of allow headers
    if (req.method === 'OPTIONS') { // if the client use method OPTION
        res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, PATCH'); 
        return res.status(200).json({});
    }
    next();
} 

