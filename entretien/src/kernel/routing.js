const path = require("path");
const fs = require("fs");

const routing = new Promise((resolve) => {
    const componentsPath = path.join(__dirname + "../../components/"); // get components path
    const componentsDir = fs.readdirSync(componentsPath); // read all directories in components directory

    // build routes 
    const routesArray = [];
    for (var i = 0; i < componentsDir.length; i++) {
        var componentPath = path.join(componentsPath + componentsDir[i] + "/"); // get component path
        var componentDir = fs.readdirSync(componentPath); // read all files in component directory
        if (componentDir.includes("routes.js")) { // if routes.js exists in this directory
            routesArray.push({
                owner: componentsDir[i], // this is component directory name
                instance: require(componentPath + "routes") // import the route file 
            });
        }
    }
    resolve(routesArray); // we return in relove method all routes 
});

module.exports = routing;