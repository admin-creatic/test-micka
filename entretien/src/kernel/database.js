const mongoose = require("mongoose");
const config = require("config");
require('dotenv').config;

// database connexion
mongoose.connect(process.env.DATABASE_URI,
    {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }
).then(() => {
    console.log("Database is connected successfully !!!");
}).catch(err => {
    console.log(err);
});